//
//  Requests.swift
//  RequestsDemo
//
//  Created by Cristian Olteanu on 13/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

class Requests {
    static func getPosts(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let urlSession = URLSession(configuration: .default)

        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")

        let dataTask = urlSession.dataTask(with: url!,
                                           completionHandler: completionHandler)

        dataTask.resume()
    }
    static func getComments(postId: Int,
                            completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        // you will need to write some code here
    }
}
