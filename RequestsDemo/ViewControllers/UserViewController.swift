//
//  UserViewController.swift
//  RequestsDemo
//
//  Created by Cristian Olteanu on 13/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    var userId: Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func pressedSeeOnMap() {
        let mapViewController = storyboard?.instantiateViewController(withIdentifier: "map")
        present(mapViewController!, animated: true, completion: nil)
    }
}
